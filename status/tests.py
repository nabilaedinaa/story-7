from django.test import TestCase, Client, LiveServerTestCase
from status.apps import StatusConfig
from django.apps import apps
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Story7UnitTest(TestCase):
    def test_index_page_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_confirm_page_url_exists(self):
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code, 200)

    def test_index_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_confirm_template_used(self):
        response = Client().get('/confirm/')
        self.assertTemplateUsed(response, 'confirm.html')

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_confirm_func(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirm)

    def test_app(self):
        self.assertEqual(StatusConfig.name, 'status')
        self.assertEqual(apps.get_app_config('status').name, 'status')
    
    def test_greeting_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)
    
    def test_confirmation_page_content(self):
        response = Client().get('/confirm/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Are you sure to submit this form?", response_content)

    def test_model_can_create_new_user(self):
        new_user = Status.objects.create(name='Edina', status='Good')
        count_all_new_user = Status.objects.all().count()
        self.assertEqual(count_all_new_user,1)
    
    def test_form_validation(self):
        new_user = StatusForm(data={'status': ''})
        self.assertFalse(new_user.is_valid())
        self.assertEqual(new_user.errors['name'], ["This field is required."])


class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest,self).tearDown()
    
    def test_functional(self) :
        self.selenium.get(self.live_server_url)
        response_content = self.selenium.page_source

        # Submit Button
        self.selenium.find_element_by_name('name').send_keys('Edina')
        self.selenium.find_element_by_name('status').send_keys('Good')
        self.selenium.find_element_by_name('submit').click()

        name = self.selenium.find_element_by_name('name').get_attribute('value')
        status = self.selenium.find_element_by_name('status').get_attribute('value')
        self.assertIn('Edina', name)
        self.assertIn('Good', status)

        self.selenium.find_element_by_name('confirm').click()
        
        response_content = self.selenium.page_source
        self.assertIn('Edina', response_content)
        self.assertIn('Good', response_content)

        # Cancel Button
        self.selenium.find_element_by_name('name').send_keys('Edina')
        self.selenium.find_element_by_name('status').send_keys('Good')
        self.selenium.find_element_by_name('submit').click()

        name = self.selenium.find_element_by_name('name').get_attribute('value')
        status = self.selenium.find_element_by_name('status').get_attribute('value')
        self.assertIn('Edina', name)
        self.assertIn('Good', status)

        self.selenium.find_element_by_name('cancel').click()

        self.assertIn('Status Board', response_content)

        # Check color-changer button
        color_before = self.selenium.find_element_by_class_name('statuss').value_of_css_property('color')
        self.assertEqual('rgba(255, 255, 255, 1)', color_before)

        self.selenium.find_element_by_id('changeColor').click()

        color_after = self.selenium.find_element_by_class_name('statuss').value_of_css_property('color')
        self.assertEqual('rgba(126, 237, 241, 1)', color_after)



        



