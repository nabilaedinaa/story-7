from django.shortcuts import render, redirect
from .forms import *
from .models import Status


# Create your views here.
def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if (form.is_valid()):
            new_name = form.cleaned_data['name']
            new_status = form.cleaned_data['status']
            response = {'name':new_name, 'status':new_status}
            return render(request, 'confirm.html', response)
    else:
        form = StatusForm()
    stats = Status.objects.all()
    response = {'stats': stats}
    return render(request, 'index.html', response)


def confirm(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if (form.is_valid()):
            new_user = Status()
            new_user.name = form.cleaned_data['name']
            new_user.status = form.cleaned_data['status']
            new_user.save()
            return redirect('/')
    else:
        form = StatusForm()
    return render(request, 'confirm.html')

