from django.db import models

# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=50,primary_key=True)
    status = models.TextField('status', max_length = 300)
    time = models.DateTimeField(auto_now_add = True, null = True)



    def __str__(self):
        return self.name