from django import forms
from .models import Status

class StatusForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'required' : True,
        'label' : '',
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'required' : True,
        'label' : '',
    }))
